/*
 Template Name: Zinzer - Responsive Bootstrap 4 Admin Dashboard
 Author: Themesdesign
 Website: www.themesdesign.in
 File: Datatable js
 */

$(document).ready(function() {
    // $('#datatable').DataTable();

    $('#datatable').dataTable( {
        order: [],
        lengthMenu: [5, 25, 50, 100, 500],
        pageLength: 5,
        scrollX: true
    } );

    $('#example').DataTable( {
            dom: 'Bfrtip',
            scrollX: true,
            buttons: [
              {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                customize: function(doc) {
                  doc.defaultStyle.fontSize = 11; //<-- set fontsize to 16 instead of 10
                }
            }
            ]
        } );

    //Buttons examples
    var table = $('#datatable-buttons').DataTable({
        lengthChange: false,
        buttons: [
          'copy', 'excel', 'pdf', 'colvis']
    });

    table.buttons().container()
        .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');
} );
