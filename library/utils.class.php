<?php
date_default_timezone_set("Asia/Bangkok");
setlocale(LC_TIME, 'id_ID');

function time_elapsed_string($timestamp, $full = false) {
  date_default_timezone_set("Asia/Kolkata");
  $time_ago        = strtotime($timestamp);
  $current_time    = time();
  $time_difference = $current_time - $time_ago;
  $seconds         = $time_difference;

  $minutes = round($seconds / 60); // value 60 is seconds
  $hours   = round($seconds / 3600); //value 3600 is 60 minutes * 60 sec
  $days    = round($seconds / 86400); //86400 = 24 * 60 * 60;
  $weeks   = round($seconds / 604800); // 7*24*60*60;
  $months  = round($seconds / 2629440); //((365+365+365+365+366)/5/12)*24*60*60
  $years   = round($seconds / 31553280); //(365+365+365+365+366)/5 * 24 * 60 * 60

  if ($seconds <= 60){

    return "Just Now";

  } else if ($minutes <= 60){

    if ($minutes == 1){

      return "one minute ago";

    } else {

      return "$minutes minutes ago";

    }

  } else if ($hours <= 24){

    if ($hours == 1){

      return "an hour ago";

    } else {

      return "$hours hrs ago";

    }

  } else if ($days <= 7){

    if ($days == 1){

      return "yesterday";

    } else {

      return "$days days ago";

    }

  } else if ($weeks <= 4.3){

    if ($weeks == 1){

      return "a week ago";

    } else {

      return "$weeks weeks ago";

    }

  } else if ($months <= 12){

    if ($months == 1){

      return "a month ago";

    } else {

      return "$months months ago";

    }

  } else {

    if ($years == 1){

      return "one year ago";

    } else {

      return "$years years ago";

    }
  }
}

function rand_color() {
    return sprintf('#%06X', mt_rand(0, 0xFFFFFF));
}

function rupiah($angka, $idr="True"){
  $angka = round($angka,0);
  $hasil_rupiah = number_format($angka,2,',','.');
  if ($idr) {
    $hasil_rupiah = "Rp " . $hasil_rupiah;
  }
	return $hasil_rupiah;
}

function jam_indo($jam)
{
  $waktu = "";
  $hours = (explode(':', $jam));
  $hour = $hours[0];
  if ($hour>=0 && $hour<=11){
    $waktu = "Pagi";
  }
  elseif ($hour >=12 && $hour<=14)  {
    $waktu = "Siang";
  }
  elseif ($hour >=15 && $hour<=17)  {
    $waktu = "Sore";
  }
  elseif ($hour >=17 && $hour<=18)  {
    $waktu = "Petang";
  }
  elseif ($hour >=19 && $hour<=23)  {
    $waktu = "Malam";
  }
  return $waktu. ', ' . $hours[0].':'.$hours[1]. ' WIB' ;
}

function tanggal_indo($tanggal, $cetak_hari = false)
{
  $tanggal = date("Y-m-d", strtotime($tanggal));
	$hari = array ( 1 =>    'Senin',
				'Selasa',
				'Rabu',
				'Kamis',
				'Jumat',
				'Sabtu',
				'Minggu'
			);

	$bulan = array (1 =>   'Januari',
				'Februari',
				'Maret',
				'April',
				'Mei',
				'Juni',
				'Juli',
				'Agustus',
				'September',
				'Oktober',
				'November',
				'Desember'
			);
	$split 	  = explode('-', $tanggal);
	$tgl_indo = $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];

	if ($cetak_hari) {
		$num = date('N', strtotime($tanggal));
		return $hari[$num] . ', ' . $tgl_indo;
	}
	return $tgl_indo;
}

function terbilang($nilai) {
    // $nilai = round($nilai,0);
		$nilai = abs($nilai);
    // echo $nilai;
		$huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
		$temp = "";
		if ($nilai < 12) {
			$temp = " ". $huruf[$nilai];
		} else if ($nilai <20) {
			$temp = terbilang($nilai - 10). " belas";
		} else if ($nilai < 100) {
			$temp = terbilang($nilai/10)." puluh". terbilang($nilai % 10);
		} else if ($nilai < 200) {
			$temp = " seratus" . terbilang($nilai - 100);
		} else if ($nilai < 1000) {
			$temp = terbilang($nilai/100) . " ratus" . terbilang($nilai % 100);
		} else if ($nilai < 2000) {
			$temp = " seribu" . terbilang($nilai - 1000);
		} else if ($nilai < 1000000) {
			$temp = terbilang($nilai/1000) . " ribu" . terbilang($nilai % 1000);
		} else if ($nilai < 1000000000) {
			$temp = terbilang($nilai/1000000) . " juta" . terbilang($nilai % 1000000);
		} else if ($nilai < 1000000000000) {
			$temp = terbilang($nilai/1000000000) . " milyar" . terbilang(fmod($nilai,1000000000));
		} else if ($nilai < 1000000000000000) {
			$temp = terbilang($nilai/1000000000000) . " trilyun" . terbilang(fmod($nilai,1000000000000));
		}
		return ucwords($temp);
	}

?>
