<div class="header-bg">
  <!-- Navigation Bar-->
  <header id="topnav">
    <div class="topbar-main">
      <div class="container-fluid">
        <div class="menu-extras topbar-custom navbar p-0">
          <ul class="list-inline ml-auto mb-0">
            <!-- User-->
            <li class="list-inline-item dropdown notification-list nav-user">
              <a class="nav-link dropdown-toggle arrow-none waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                <img src="assets/images/logors.png" alt="user" class="rounded-circle">
                <span class="d-none d-md-inline-block ml-1">Menu Administrator<i class="mdi mdi-chevron-down"></i> </span>
              </a>
              <div class="dropdown-menu dropdown-menu-right dropdown-menu-animated profile-dropdown">
                <a class="dropdown-item" href="keperluan"><i class="dripicons-tag text-muted"></i> Master Keperluan</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="admin"><i class="dripicons-pulse text-muted"></i> Laporan Grafik</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="bukutamu"><i class="dripicons-briefcase text-muted"></i> Buku Tamu</a>
                <a class="dropdown-item" href="kunjunganwaktu"><i class="dripicons-clock text-muted"></i> Berdasarkan Waktu</a>
                <a class="dropdown-item" href="kunjungankecamatan"><i class="dripicons-map text-muted"></i> Berdasarkan Kecamatan</a>
                <a class="dropdown-item" href="kunjunganpekerjaan"><i class="dripicons-graduation text-muted"></i> Bedasarkan Pekerjaan</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="cetak"><i class="dripicons-monitor text-muted"></i> Cetak Laporan</a>

              </div>
            </li>

          </ul>

        </div>
        <!-- end menu-extras -->

        <div class="clearfix"></div>

      </div> <!-- end container -->
    </div>
    <!-- end topbar-main -->
  </header>
  <!-- End Navigation Bar-->

</div>

<div class="row m-t-10">
  <div class="col-sm-12">
    <div class="page-title-box">
      <div class="row align-items-center">
        <div class="col-md-8">
          <h4 class="page-title m-0">Keperluan Kunjungan</h4>
        </div>
        <div class="col-md-4">
          <div class="float-right d-none d-md-block">
            <div class="text-center">
              <button type="button" class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target=".bs-example-modal-center">Tambah Jenis Kunjungan</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <?php
  if (isset($data["error"]) && count($data["error"]) > 0) {
    ?>
    <div class="alert alert-solid alert-danger" role="alert">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <?php
        foreach ($data["error"] as $error) {
          ?>
        <?php echo "| " . $error; ?>
      <?php
        } ?>
    </div>
  <?php
  } elseif (isset($data["success"])) {
    ?>
    <script>
      swal({
        title: 'Proses Berhasil',
        type: 'warning',
        onfirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger m-l-10'
      })
    </script>
    <meta http-equiv="refresh" content="2;url=">
  <?php
  } ?>
  <div class="col-12">
    <div class="card m-b-30">
      <div class="card-body">
        <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
          <thead>
            <tr>
              <th>Deskripsi Keperluan</th>
              <th>Minimal Umur</th>
              <th>Status</th>
              <th>Tindakan</th>
            </tr>
          </thead>

          <tbody>
            <?php foreach ($data["kpl"] as $val) :
              ?>
              <tr>
                <td><?php echo $val->keperluan ?></td>
                <td>
                  <?php if ($val->minimalumur != 0) : ?>
                    <?php echo $val->minimalumur . ' Tahun ' ?>
                  <?php else : ?>
                    Tidak Ada Batasan Umur
                  <?php endif; ?>
                </td>
                <td class="text-center">
                  <?php if ($val->status == 1) : ?>
                    <span class="badge badge-primary">Aktif</span>
                  <?php else : ?>
                    <span class="badge badge-danger">Non-Aktif</span>
                  <?php endif; ?>
                </td>
                <td>
                  <?php if ($val->status == 1) : ?>
                    <a href="<?php echo SITE_URL; ?>?page=keperluan&&action=setNonActive&&id=<?php echo $val->id_keperluan; ?>" type="button" class="btn btn-danger waves-effect waves-light">Non- Aktifkan</a>
                  <?php else : ?>
                    <a href="<?php echo SITE_URL; ?>?page=keperluan&&action=setActive&&id=<?php echo $val->id_keperluan; ?>" type="button" class="btn btn-primary waves-effect waves-light">Aktifkan</a>
                  <?php endif; ?>
                  <a type="button" href='#myModal1' class="btn btn-success waves-effect waves-light" data-toggle="modal" data-target=".bs-example-modal-center1" data-id="<?php echo $val->id_keperluan . '-' . $val->minimalumur . '-' . $val->keperluan ?>">Edit Data</a>
                  <a href="<?php echo SITE_URL; ?>?page=keperluan&&action=delete&&id=<?php echo $val->id_keperluan; ?>" type="button" class="btn btn-warning waves-effect waves-light">Hapus</a>

                </td>
              </tr>
            <?php endforeach; ?>
          </tbody>
        </table>

      </div>
    </div>
  </div> <!-- end col -->
  <!-- Insert -->
  <div class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title mt-0">Tambah Jenis Kunjungan</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form class="" action="<?php echo SITE_URL; ?>?page=keperluan&&action=insert" method="post">
            <div class="form-group row">
              <label for="example-text-input" class="col-sm-2 col-form-label">Deksripsi Kunjungan</label>
              <div class="col-sm-10">
                <input class="form-control" name="des" type="text">
              </div>
            </div>
            <div class="form-group row">
              <label for="example-search-input" class="col-sm-2 col-form-label">Umur Minimal</label>
              <div class="col-sm-10">
                <input class="form-control" type="text" name="umur">
                <code>* Biarkan Bila Tidak Ada Batasan Umur</code>
              </div>
            </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-success">Simpan</button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </form>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
  <!-- Update -->
  <div class="modal fade bs-example-modal-center1" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title mt-0">Edit Jenis Kunjungan</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form class="" action="<?php echo SITE_URL; ?>?page=keperluan&&action=update" method="post">
            <div class="form-group row">
              <label for="example-text-input" class="col-sm-2 col-form-label">Kunjungan</label>
              <div class="col-sm-10">
                <input class="form-control" id="id" name="id" type="hidden">
                <input class="form-control" id="des" required name="des" type="text">
              </div>
            </div>
            <div class="form-group row">
              <label for="example-search-input" class="col-sm-2 col-form-label">Min</label>
              <div class="col-sm-10">
                <input class="form-control" id="umur" type="text" name="umur">
                <code>* Umur Minimal, Kosongkan Bila Tidak Ada Batasan Umur</code>
              </div>
            </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-success">Simpan</button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </form>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
</div> <!-- end row -->

<script type="text/javascript">
  $(document).ready(function() {
    console.log("sasas");
    $('#myModal1').on('show.bs.modal', function(e) {
      var rowid = $(e.relatedTarget).data('id');
      console.log(rowid);
      var res = rowid.split("-");
      $("#id").val(res[0]);
      $("#des").val(res[2]);
      $("#umur").val(res[1]);
    });
  });
</script>