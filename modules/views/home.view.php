<br>
<div class="card m-t-10 center-block">
  <div class="card-body">
    <div class="media m-t-5 m-b-5">
      <img class="d-flex mr-3" src="assets/images/logors.png" alt="" height="64">
      <div class="media-body m-t-10 center-block">
       <h4 class="card-title font-18 mt-0">Sistem Informasi Buku Tamu</h4>
       <h6 class="card-subtitle font-12 text-muted">Aplikasi Pencatatan Kunjungan Dan Buku Tamu Syamrabu</h6>
      </div>
    </div>
  </div>
  <div id="carouselExampleIndicators" class="carousel slide" data-interval="3000" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner" role="listbox">
      <div class="carousel-item active">
        <img class="img-fluid " src="assets/images/slide1.jpg" alt="First slide">
      </div>
      <div class="carousel-item">
        <img class="img-fluid " src="assets/images/slide2.jpg" alt="Second slide">
      </div>
      <div class="carousel-item">
        <img class="img-fluid " src="assets/images/slide3.jpg" alt="Third slide">
      </div>
    </div>
    <!-- <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a> -->
  </div>
  <!-- <img class="img-fluid" src="assets/images/small/img-4.jpg" alt="Card image cap"> -->
  <div class="card-body text-center">
    <!-- <p class="card-text">Bantu kami untuk mengetahui layanan yang dibutuhkan anda dengan mengisi buku tamu secara online melaui aplikasi ini. </p> -->
    <a href="tamu" class="btn btn-lg btn-success btn-block waves-effect waves-light">MULAI UNTUK MENGISI BUKU TAMU KUNJUNGAN</a>
  </div>
</div>
