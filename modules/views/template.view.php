<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <title>Sikuta Syamrabu</title>
    <meta content="Admin Dashboard" name="description" />
    <meta content="ThemeDesign" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!-- App Icons -->
    <!-- <link rel="shortcut icon" href="assets/images/favicon.ico"> -->


    <!-- App css -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/signature-pad.css" rel="stylesheet" type="text/css" />
    <link href="resource/sweet-alert2/sweetalert2.css" rel="stylesheet" type="text/css">


    <!-- DataTables -->
    <link href="resource/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="resource/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />

    <!-- Responsive datatable examples -->
    <link href="resource/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />


    <!-- jQuery  -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.bundle.min.js"></script>
    <script src="assets/js/modernizr.min.js"></script>
    <script src="assets/js/waves.js"></script>
    <script src="assets/js/jquery.slimscroll.js"></script>



    <!-- Required datatable js -->
    <script src="resource/datatables/jquery.dataTables.min.js"></script>
    <script src="resource/datatables/dataTables.bootstrap4.min.js"></script>

    <!-- Buttons examples -->
    <script src="resource/datatables/dataTables.buttons.min.js"></script>
    <script src="resource/datatables/buttons.bootstrap4.min.js"></script>
    <script src="resource/datatables/jszip.min.js"></script>
    <script src="resource/datatables/pdfmake.min.js"></script>
    <script src="resource/datatables/vfs_fonts.js"></script>
    <script src="resource/datatables/buttons.html5.min.js"></script>
    <script src="resource/datatables/buttons.print.min.js"></script>
    <script src="resource/datatables/buttons.colVis.min.js"></script>


    <!-- Responsive examples -->
    <script src="resource/datatables/dataTables.responsive.min.js"></script>
    <script src="resource/datatables/responsive.bootstrap4.min.js"></script>

    <script src="resource/chartjs/chart.min.js"></script>
    <script type="text/javascript" src="assets/pages/chartjs.init.js"></script>

    <script type="text/javascript" src="resource/jsPDF/dist/jspdf.min.js"></script>
    <!-- Datatable init js -->

    <!-- Sweet-Alert  -->
    <script src="resource/sweet-alert2/sweetalert2.min.js"></script>
    <script src="assets/pages/sweet-alert.init.js"></script>

    <!-- dashboard js -->
    <script src="assets/pages/dashboard.int.js"></script>

    <!-- App js -->
    <script src="assets/js/app.js"></script>
    <script src="assets/js/signature_pad.js"></script>


</head>


<body>

    <!-- Loader -->
    <div id="preloader">
        <div id="status">
            <!-- <a href="" class="logo logo-admin"><img src="assets/images/logors.png" height="128" alt="logo"></a> -->
            <div class="spinner">
                <div class="rect1"></div>
                <div class="rect2"></div>
                <div class="rect3"></div>
                <div class="rect4"></div>
                <div class="rect5"></div>
            </div>
        </div>
    </div>

    <div class="wrapper">
        <div class="container-fluid">
            <!-- end page title end breadcrumb -->
            <?php
            $view = new View($viewName);
            $view->bind('data', $data);
            $view->forceRender();
            ?>
            <!-- end row -->
        </div>
    </div>
    <!-- end wrapper -->


    <!-- Footer -->
    <!-- <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    © 2019 IT SYAMRABU <span class="d-none d-md-inline-block"> - Build with <i class="mdi mdi-heart text-danger"></i> by fianeka.me</span>
                </div>
            </div>
        </div>
    </footer> -->
    <!-- End Footer -->

<script>
var a=document.getElementsByTagName("a");
for(var i=0;i<a.length;i++) {
    if(!a[i].onclick && a[i].getAttribute("target") != "_blank") {
        a[i].onclick=function() {
                window.location=this.getAttribute("href");
                return false;
        }
    }
}
</script>
</body>
<script src="assets/pages/datatables.init.js"></script>

</html>
