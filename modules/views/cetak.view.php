<div class="header-bg">
    <!-- Navigation Bar-->
    <header id="topnav">
        <div class="topbar-main">
            <div class="container-fluid">
                <div class="menu-extras topbar-custom navbar p-0">
                    <ul class="list-inline ml-auto mb-0">
                        <!-- User-->
                        <li class="list-inline-item dropdown notification-list nav-user">
                            <a class="nav-link dropdown-toggle arrow-none waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                <img src="assets/images/logors.png" alt="user" class="rounded-circle">
                                <span class="d-none d-md-inline-block ml-1">Menu Administrator<i class="mdi mdi-chevron-down"></i> </span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-animated profile-dropdown">
                                <a class="dropdown-item" href="keperluan"><i class="dripicons-tag text-muted"></i> Master Keperluan</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="admin"><i class="dripicons-pulse text-muted"></i> Laporan Grafik</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="bukutamu"><i class="dripicons-briefcase text-muted"></i> Buku Tamu</a>
                                <a class="dropdown-item" href="kunjunganwaktu"><i class="dripicons-clock text-muted"></i> Berdasarkan Waktu</a>
                                <a class="dropdown-item" href="kunjungankecamatan"><i class="dripicons-map text-muted"></i> Berdasarkan Kecamatan</a>
                                <a class="dropdown-item" href="kunjunganpekerjaan"><i class="dripicons-graduation text-muted"></i> Bedasarkan Pekerjaan</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="cetak"><i class="dripicons-monitor text-muted"></i> Cetak Laporan</a>

                            </div>
                        </li>

                    </ul>

                </div>
                <!-- end menu-extras -->

                <div class="clearfix"></div>

            </div> <!-- end container -->
        </div>
        <!-- end topbar-main -->
    </header>
    <!-- End Navigation Bar-->

</div>
<div class="row m-t-10">
    <div class="col-sm-12">
        <div class="page-title-box">
            <div class="row align-items-center">
                <div class="col-md-8">
                    <h4 class="page-title m-0">Cetak Laporan Kunjungan</h4>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card m-b-10">
            <div class="card-body">
                <form class="" method="post">
                    <div class="form-group">
                      <div class="row">
                        <div class="col-lg-4">
                          <label>Tanggal Atau</label>  Pilih Semua Hari <input type="checkbox" id="myCheckbox" name="tglall" value="all">
                          <input class="form-control " name="tgl" type="date" id="tglnya">
                        </div>
                        <div class="col-lg-4">
                          <label>Kecamatan</label>
                              <select name="kec" id="kec" class="form-control">
                                  <option value="All">Semua Daerah</option>
                                  <option value="Daerah Lain">Daerah Lain</option>
                                  <option value="Arosbaya">Arosbaya</option>
                                  <option value="Bangkalan">Bangkalan</option>
                                  <option value="Blega">Blega</option>
                                  <option value="Burneh">Burneh</option>
                                  <option value="Galis">Galis</option>
                                  <option value="Geger">Geger</option>
                                  <option value="Kamal">Kamal</option>
                                  <option value="Klampis">Klampis</option>
                                  <option value="Kokop">Kokop</option>
                                  <option value="Konang">Konang</option>
                                  <option value="Kwanyar">Kwanyar</option>
                                  <option value="Labang">Labang</option>
                                  <option value="Modung">Modung</option>
                                  <option value="Sepulu">Sepulu</option>
                                  <option value="Socah">Socah</option>
                                  <option value="Tanah Merah">Tanah Merah</option>
                                  <option value="Tanjung Bumi">Tanjung Bumi</option>
                                  <option value="Tragah">Tragah</option>
                              </select>
                        </div>
                        <div class="col-lg-4">
                          <label>Pekerjaan</label>
                          <select name="pekerjaan" id="pekerjaan" class="form-control">
                            <option value="All">Semua Pekerjaan</option>
                              <option value="Lain-Lain">Lainnya</option>
                              <option value="Pegawai Negeri Sipil">Pegawai Negeri Sipil</option>
                              <option value="Swasta">Swasta</option>
                              <option value="TNI">TNI</option>
                              <option value="POLRI">POLRI</option>
                              <option value="Karyawan BUMN">Karyawan BUMN</option>
                              <option value="Pelajar">Pelajar</option>
                              <option value="Mahasiswa">Mahasiswa</option>
                              <option value="Tani">Tani</option>
                              <option value="Buruh">Buruh</option>
                              <option value="Nelayan">Nelayan</option>
                          </select>
                        </div>
                      </div>
                        <div>
                            <button type="submit" class="btn m-t-10 btn-block btn-primary waves-effect waves-light">Cek Data Kunjungan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php if (!empty($data["bukutamu"])) : ?>
    <div class="row">
        <div class="col-12">
            <div class="card m-b-30">
                <div class="card-body">
                  <div class="pull-right">
                </div>
                    <table id="example" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead class="">
                            <tr>
                                <th>Nama</th>
                                <th>Umur</th>
                                <th>Jenis Kelamin</th>
                                <th>Telpon</th>
                                <th>Pekerjaan</th>
                                <th>Kecamatan</th>
                                <th>Alamat</th>
                                <th>Tanggal Kunjungan</th>
                                <th>Jam Kunjungan</th>
                                <th>Keperluan</th>
                                <th>Ttd</th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php foreach ($data["bukutamu"] as $val) :
                                    ?>
                                <tr>
                                    <td><?php echo $val->nama ?></td>
                                    <td><?php
                                      $interval = date_diff(date_create(), date_create($val->tgl_lahir));
                                      echo $interval->format("%Y Tahun");
                                    ?></td>
                                    <td><?php echo $val->jk ?></td>
                                    <td><?php echo $val->notel ?></td>
                                    <td><?php echo $val->pekerjaan ?></td>
                                    <td><?php echo $val->kecamatan ?></td>
                                    <td><?php echo $val->alamat ?></td>
                                    <td><?php echo tanggal_indo($val->waktu_kunjungan, false) ?></td>
                                    <td><?php echo $val->jam_kunjungan ?></td>
                                    <td><?php echo $val->keperluan ?></td>
                                    <td><img src="<?php echo $val->ttd ?>" height="24"/></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>

                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->


    </div> <!-- end row -->
<?php else : ?>

<?php endif; ?>
<script>
const checkbox = document.getElementById('myCheckbox')
checkbox.addEventListener('change', (event) => {
  if (event.target.checked) {
    document.getElementById("tglnya").disabled = true; 
    document.getElementById("tglnya").value = ""; 
  } else {
    document.getElementById("tglnya").disabled = false; 
  }
})
</script>