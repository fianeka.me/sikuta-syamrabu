<div class="header-bg">
    <!-- Navigation Bar-->
    <header id="topnav">
        <div class="topbar-main">
            <div class="container-fluid">
                <div class="menu-extras topbar-custom navbar p-0">
                    <ul class="list-inline ml-auto mb-0">
                        <!-- User-->
                        <li class="list-inline-item dropdown notification-list nav-user">
                            <a class="nav-link dropdown-toggle arrow-none waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                <img src="assets/images/logors.png" alt="user" class="rounded-circle">
                                <span class="d-none d-md-inline-block ml-1">Menu Administrator<i class="mdi mdi-chevron-down"></i> </span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-animated profile-dropdown">
                                <a class="dropdown-item" href="keperluan"><i class="dripicons-tag text-muted"></i> Master Keperluan</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="admin"><i class="dripicons-pulse text-muted"></i> Laporan Grafik</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="bukutamu"><i class="dripicons-briefcase text-muted"></i> Buku Tamu</a>
                                <a class="dropdown-item" href="kunjunganwaktu"><i class="dripicons-clock text-muted"></i> Berdasarkan Waktu</a>
                                <a class="dropdown-item" href="kunjungankecamatan"><i class="dripicons-map text-muted"></i> Berdasarkan Kecamatan</a>
                                <a class="dropdown-item" href="kunjunganpekerjaan"><i class="dripicons-graduation text-muted"></i> Bedasarkan Pekerjaan</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="cetak"><i class="dripicons-monitor text-muted"></i> Cetak Laporan</a>

                            </div>
                        </li>

                    </ul>

                </div>
                <!-- end menu-extras -->

                <div class="clearfix"></div>

            </div> <!-- end container -->
        </div>
        <!-- end topbar-main -->
    </header>
    <!-- End Navigation Bar-->

</div>

<div class="row m-t-10">
    <div class="col-sm-12">
        <div class="page-title-box">
            <div class="row align-items-center">
                <div class="col-md-8">
                    <h4 class="page-title m-0">Laporan Kunjungan Berdasarkan Pekerjaan</h4>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card m-b-10">
            <div class="card-body">
                <h4 class="mt-0 header-title">Pencarian Berdasarkan Jenis Pekerjaan Pegunjung</h4>
                <p class="text-muted m-b-10 font-14">Data akan difilter menggunakan kunci sesuai dengan pekerjaan dibawah ini</p>
                <form class="" method="post">
                    <div class="form-group">
                        <div>
                            <select name="pekerjaan" id="pekerjaan" class="form-control">
                                <option value="Lain-Lain">Lainnya</option>
                                <option value="Pegawai Negeri Sipil">Pegawai Negeri Sipil</option>
                                <option value="Swasta">Swasta</option>
                                <option value="TNI">TNI</option>
                                <option value="POLRI">POLRI</option>
                                <option value="Karyawan BUMN">Karyawan BUMN</option>
                                <option value="Pelajar">Pelajar</option>
                                <option value="Mahasiswa">Mahasiswa</option>
                                <option value="Tani">Tani</option>
                                <option value="Buruh">Buruh</option>
                                <option value="Nelayan">Nelayan</option>
                            </select>
                        </div>
                        <div>
                            <button type="submit" class="btn m-t-10 btn-block btn-primary waves-effect waves-light">Cek Data Kunjungan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php if (!empty($data["bukutamu"])) : ?>
    <div class="row">
        <div class="col-12">
            <div class="card m-b-30">
                <div class="card-body">
                    <h3 class="mt-0 header-title">Total Kunjungan Pengunjung Dengan Jenis Pekerjaan <i> <?php echo $data['job']; ?> </i> Berjumlah : <i><?php echo count($data["bukutamu"]); ?></i> Orang </h3>
                    <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead class="">
                            <tr>
                                <th>Nama</th>
                                <th>Jenis Kelamin</th>
                                <th>Pekerjaan</th>
                                <th>Asal</th>
                                <th>Tanggal Kunjungan</th>
                                <th>Jam Kunjungan</th>
                                <th>Tindakan</th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php foreach ($data["bukutamu"] as $val) :
                                    ?>
                                <tr>
                                    <td><?php echo $val->nama ?></td>
                                    <td><?php echo $val->jk ?></td>
                                    <td><?php echo $val->pekerjaan ?></td>
                                    <td><?php echo $val->kecamatan ?></td>
                                    <td><?php echo tanggal_indo($val->waktu_kunjungan, true) ?></td>
                                    <td><?php echo jam_indo($val->jam_kunjungan) ?></td>
                                    <td align="center">
                                        <a type="button" href='#myModal' class="btn btn-success waves-effect waves-light" data-toggle="modal" data-target=".bs-example-modal-center" data-id="<?php echo $val->id ?>">Lihat Detail</a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>

                        <tfoot class="">
                            <tr>
                                <th>Nama</th>
                                <th>Jenis Kelamin</th>
                                <th>Pekerjaan</th>
                                <th>Asal</th>
                                <th>Tanggal Kunjungan</th>
                                <th>Jam Kunjungan</th>
                                <th>Tindakan</th>
                            </tr>
                        </tfoot>
                    </table>

                </div>
            </div>
        </div> <!-- end col -->
        <div class="modal fade bs-example-modal-center" id="myModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title mt-0">Detail Kunjungan</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="fetched-data"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" data-dismiss="modal">Tutup</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </div> <!-- end row -->

    <script type="text/javascript">
        $(document).ready(function() {
            $('#myModal').on('show.bs.modal', function(e) {
                var rowid = $(e.relatedTarget).data('id');
                //menggunakan fungsi ajax untuk pengambilan data
                $.ajax({
                    type: 'post',
                    url: 'modules/custommodul/detailkunjungan.php',
                    data: 'rowid=' + rowid,
                    success: function(data) {
                        $('.fetched-data').html(data); //menampilkan data ke dalam modal
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                        alert(errorThrown);
                    }
                });
            });
        });
    </script>
    </div> <!-- end row -->
<?php else : ?>

<?php endif; ?>