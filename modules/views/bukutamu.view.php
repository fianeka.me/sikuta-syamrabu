<div class="card center-block">
  <?php
  if (isset($data["error"]) && count($data["error"]) > 0) {
    ?>
    <div class="alert alert-solid alert-danger" role="alert">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <?php
        foreach ($data["error"] as $error) {
          ?>
        <?php echo "| " . $error; ?>
      <?php
        } ?>
    </div>
  <?php
  } elseif (isset($data["success"])) {
    ?>
    <script>
      swal({
        title: 'Terimakasih!',
        text: 'Telah Melakukan Pengisian Buku Tamu Kunjungan RSUD!',
        type: 'success',
        onfirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger m-l-10'
      })
    </script>
    <meta http-equiv="refresh" content="3;url=home">
  <?php
  } ?>
  <div class="card-body">
    <div class="media m-t-0">
      <a href="home" class="logo">
        <img class="d-flex mr-2" src="assets/images/logors.png" alt="" height="64">
      </a>
      <div class="media-body m-t-10 center-block">
        <h4 class="card-title font-18 mt-0">Sistem Informasi Buku Tamu
        </h4>
        <h6 class="card-subtitle font-12 text-muted">Aplikasi Pencatatan Kunjungan Dan Buku Tamu Syamrabu</h6>
      </div>
      <!-- <img class="d-flex mr-2" src="assets/images/logors.png" alt="" height="64"> -->
      <button type="button" class="btn btn-success waves-effect m-t-10 waves-light" data-toggle="modal" data-target=".bs-example-modal-lg">Ambil Data Dari SIM-RS</button>
    </div>
    <div class="modal fade bs-example-modal-lg" id="modalnya" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title mt-0" id="myLargeModalLabel"> Cari Data <small><span class="badge badge-info">Gunakan Minimal 3 Huruf Untuk Keyword</span></small></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body text-center">
            <div class="form-group row">
              <div class="col-sm-10">
                  <input class="form-control  form-control-lg" id="keyword" minlength="3" pe="text" placeholder="Masukan Keyword Berupa NRM / NAMA" >
              </div>
              <div class="col-sm-2">
                  <button type="button" id="btncari" class="btn btn-lg btn-block btn-success">Cari</button>
              </div>
            </div>
            <hr>
            <button class="loading btn btn-pink text-center" type="button">
              <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
              Mohon Menunggu, Sedang Dilakukan Pencarian
            </button>
            <div class="fetched-data">


          </div>
        </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <hr>
    <form class="" method="post">
      <div class="form-group">
        <div class="row">
          <div class="col-sm-6 col-lg-6 col-md-6">
            <label>Nomor Rekam Medik</label>
            <input type="text" name="nrm" id="nrm" class="form-control" placeholder="Kosongkan Bila Tidak Ada" />
          </div>
          <div class="col-sm-6 col-lg-6 col-md-6">
            <label>Nama</label>
            <input type="text" name="nama" id="nama" class="form-control" required placeholder="Contoh : Moh. Abdullah" />
          </div>
        </div>
      </div>
      <div class="form-group">
        <div class="row">
          <div class="col-sm-6 col-lg-6 col-md-6">
            <label>Tanggal Lahir</label>
            <input class="form-control" id="tgl" type="date" onchange="setKeperluan(event);" value="1990-01-01" name="tgllahir">
            <!-- <code id="umur" class="highlighter-rouge">Umur Anda : </code> -->
          </div>
          <div class="col-sm-6 col-lg-6 col-md-6">
            <label>Nomor Telphone</label>
            <input type="number" id="notel" name="notel" class="form-control" placeholder="Boleh Dikosongkan" />
          </div>
        </div>
      </div>
      <div class="form-group">
        <div class="row">
          <div class="col-sm-6 col-lg-6 col-md-6">
            <label>Jenis Kelamin</label>
            <select name="jk" id="jk" class="form-control">
              <option value="Laki-Laki">Laki - Laki</option>
              <option value="Perempuan">Perempuan</option>
            </select>
          </div>
          <div class="col-sm-6 col-lg-6 col-md-6">
            <label>Pekerjaan</label>
            <select name="pekerjaan" id="pekerjaan" class="form-control">
              <option value="Lain-Lain">Lainnya</option>
              <option value="Pegawai Negeri Sipil">Pegawai Negeri Sipil</option>
              <option value="Swasta">Swasta</option>
              <option value="TNI">TNI</option>
              <option value="POLRI">POLRI</option>
              <option value="Karyawan BUMN">Karyawan BUMN</option>
              <option value="Pelajar">Pelajar</option>
              <option value="Mahasiswa">Mahasiswa</option>
              <option value="Tani">Tani</option>
              <option value="Buruh">Buruh</option>
              <option value="Nelayan">Nelayan</option>
            </select>
          </div>
        </div>
      </div>
      <div class="form-group">
        <div class="row">
          <div class="col-sm-6 col-lg-6 col-md-6">
            <label>Kecamatan</label>
            <select name="kec" id="kec" class="form-control">
              <option value="Daerah Lain">Daerah Lain</option>
              <option value="Arosbaya">Arosbaya</option>
              <option value="Bangkalan">Bangkalan</option>
              <option value="Blega">Blega</option>
              <option value="Burneh">Burneh</option>
              <option value="Galis">Galis</option>
              <option value="Geger">Geger</option>
              <option value="Kamal">Kamal</option>
              <option value="Klampis">Klampis</option>
              <option value="Kokop">Kokop</option>
              <option value="Konang">Konang</option>
              <option value="Kwanyar">Kwanyar</option>
              <option value="Labang">Labang</option>
              <option value="Modung">Modung</option>
              <option value="Sepulu">Sepulu</option>
              <option value="Socah">Socah</option>
              <option value="Tanah Merah">Tanah Merah</option>
              <option value="Tanjung Bumi">Tanjung Bumi</option>
              <option value="Tragah">Tragah</option>
            </select>
          </div>
          <div class="col-sm-6 col-lg-6 col-md-6">
            <label>Keperluan</label>
            <select id="kl" name="keperluan" class="form-control">
            </select>
          </div>
        </div>
      </div>
      <div class="form-group">
        <div class="row">
          <div class="col-sm-6 col-lg-6 col-md-6">
            <label>Alamat</label>
            <textarea name="alamat" id="alamat" class="form-control" rows="6"></textarea>
          </div>
          <!-- <input type="text" name="alamat" class="form-control" required placeholder="Contoh : Jalan Ir. Soekarno 06 Bangkalan" /> -->
          <div class="col-sm-6 col-lg-6 col-md-6">
            <label>Tanda Tangan</label>
            <div id="signature-padi" class="m-signature-pad">
              <div class="m-signature-pad--footer">
                <input id="ttd" required name="ttd" type="text">
                <button id="save" type="button" data-action="save">Validasi</button>
                <button type="button" class="clear" data-action="clear">Hapus</button>
              </div>
              <div class="m-signature-pad--body">
                <canvas></canvas>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="form-group">
        <div class="row">
          <!-- <div class="col-sm-7 col-lg-7 col-md-7">
            <label>Keterangan <code>(optional)</code></label>
            <textarea name="ket" class="form-control" rows="3"></textarea>
          </div> -->

        </div>
      </div>
      <button type="submit" id="myBtn" class="btn btn-lg btn-success btn-block waves-effect waves-light" disabled>Simpan Data Kunjungan</button>
  </div>
  </form>
</div>
</div>


<?php
$grapdata = array();
$id = array();
$keperluan = array();
$minimal = array();
foreach ($data["keperluan"] as $val) {
  array_push($id, $val->id_keperluan);
  array_push($keperluan, $val->keperluan);
  array_push($minimal, $val->minimalumur);
}
array_push($grapdata, $id, $keperluan, $minimal);
?>
<script type="text/javascript">
  var grap1 = <?php echo json_encode($grapdata); ?>;
</script>

<script>
  function setKeperluan(e) {
    $("#kl").empty()
    var mdate = $('#tgl').val().toString();
    var yearThen = parseInt(mdate.substring(0, 4), 10);
    var monthThen = parseInt(mdate.substring(5, 7), 10);
    var dayThen = parseInt(mdate.substring(8, 10), 10);

    var today = new Date();
    var birthday = new Date(yearThen, monthThen - 1, dayThen);

    var differenceInMilisecond = today.valueOf() - birthday.valueOf();

    var year_age = Math.floor(differenceInMilisecond / 31536000000);
    var day_age = Math.floor((differenceInMilisecond % 31536000000) / 86400000);

    var month_age = Math.floor(day_age / 30);
    day_age = day_age % 30;

    for (i = 0; i < grap1[0].length; i++) {
      if (year_age >= grap1[2][i]) {
        var x = document.getElementById("kl");
        var option = document.createElement("option");
        option.text = grap1[1][i];
        option.value = grap1[0][i];
        x.add(option);
      }
    }
  }
</script>



<script>
  var wrapper = document.getElementById("signature-padi"),
    clearButton = wrapper.querySelector("[data-action=clear]"),
    saveButton = wrapper.querySelector("[data-action=save]"),
    canvas = wrapper.querySelector("canvas"),
    signaturePad;

  // Adjust canvas coordinate space taking into account pixel ratio,
  // to make it look crisp on mobile devices.
  // This also causes canvas to be cleared.
  function resizeCanvas() {
    // When zoomed out to less than 100%, for some very strange reason,
    // some browsers report devicePixelRatio as less than 1
    // and only part of the canvas is cleared then.
    var ratio = Math.max(window.devicePixelRatio || 1, 1);
    canvas.width = canvas.offsetWidth * ratio;
    canvas.height = canvas.offsetHeight * ratio;
    canvas.getContext("2d").scale(ratio, ratio);
  }

  window.onresize = resizeCanvas;
  resizeCanvas();

  signaturePad = new SignaturePad(canvas);

  clearButton.addEventListener("click", function(event) {
    document.getElementById("ttd").value = "";
    document.getElementById("myBtn").disabled = true;
    signaturePad.clear();
  });

  saveButton.addEventListener("click", function(event) {
    document.getElementById("ttd").value = signaturePad.toDataURL();
    document.getElementById("myBtn").disabled = false;
  });
</script>


<script type="text/javascript">
    $('.loading').hide();
    $(document).ready(function() {
      $("#btncari").click(function(){
        if ($("#keyword").val().length >= 3){
        var rowid = $("#keyword").val();
          $.ajax({
              type: 'post',
              url: 'modules/custommodul/patient_simrs.php',
              data: 'rowid=' + rowid,
              beforeSend: function(){
                  $('.loading').show();
                  $('.fetched-data').hide();
              },
              complete: function(){
                  $('.loading').hide();
                  $('.fetched-data').show();
              },
              success: function(data) {
                  $('.fetched-data').html(data); //menampilkan data ke dalam modal
              },
              error: function(XMLHttpRequest, textStatus, errorThrown) {
                  alert(errorThrown);
              }
          });
        }
      });
    });
</script>
