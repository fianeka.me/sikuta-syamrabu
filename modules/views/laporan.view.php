<div class="header-bg">
    <!-- Navigation Bar-->
    <header id="topnav">
        <div class="topbar-main">
            <div class="container-fluid">
                <div class="menu-extras topbar-custom navbar p-0">
                    <ul class="list-inline ml-auto mb-0">
                        <!-- User-->
                        <li class="list-inline-item dropdown notification-list nav-user">
                            <a class="nav-link dropdown-toggle arrow-none waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                <img src="assets/images/logors.png" alt="user" class="rounded-circle">
                                <span class="d-none d-md-inline-block ml-1">Menu Administrator<i class="mdi mdi-chevron-down"></i> </span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-animated profile-dropdown">
                                <a class="dropdown-item" href="keperluan"><i class="dripicons-tag text-muted"></i> Master Keperluan</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="admin"><i class="dripicons-pulse text-muted"></i> Laporan Grafik</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="bukutamu"><i class="dripicons-briefcase text-muted"></i> Buku Tamu</a>
                                <a class="dropdown-item" href="kunjunganwaktu"><i class="dripicons-clock text-muted"></i> Berdasarkan Waktu</a>
                                <a class="dropdown-item" href="kunjungankecamatan"><i class="dripicons-map text-muted"></i> Berdasarkan Kecamatan</a>
                                <a class="dropdown-item" href="kunjunganpekerjaan"><i class="dripicons-graduation text-muted"></i> Bedasarkan Pekerjaan</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="cetak"><i class="dripicons-monitor text-muted"></i> Cetak Laporan</a>

                            </div>
                        </li>

                    </ul>

                </div>
                <!-- end menu-extras -->

                <div class="clearfix"></div>

            </div> <!-- end container -->
        </div>
        <!-- end topbar-main -->
    </header>
    <!-- End Navigation Bar-->

</div>

<div class="row m-t-10">
    <div class="col-sm-12">
        <div class="page-title-box">
            <div class="row align-items-center">
                <div class="col-md-8">
                    <h4 class="page-title m-0">Laporan Grafik Kunjungan Tamu</h4>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xl-4">
        <div class="card">
            <div class="card-body">
                <h4 class="mt-0 header-title mb-4">Kunjungan Terbaru</h4>
                <ol class="activity-feed mb-0">
                    <?php foreach ($data["data"] as $val) :
                        ?>
                        <li class="feed-item">
                            <div class="feed-item-list">
                                <span class="date text-white-50"><?php echo tanggal_indo($val->waktu_kunjungan, true); ?> Pada</span>
                                <span class="date text-white-50"><?php echo jam_indo($val->jam_kunjungan); ?></span>
                                <span class="activity-text text-white">
                                    <b><i><?php echo $val->nama . " </i></b> berkunjung untuk " . $val->keperluan ?>
                                </span>
                            </div>
                        </li>
                    <?php endforeach; ?>
                </ol>
            </div>
        </div>
    </div>
    <div class="col-xl-8">
        <div class="row">
            <div class="col-xl-6">
                <div class="col-xl-12">
                    <div class="card m-b-30">
                        <div class="card-body">
                            <h4 class="mt-0 header-title">Berdasarkan Jenis Kelamin</h4>
                            <div class="table-responsive">
                                <table class="table table-bordered mb-0">
                                    <thead>
                                        <tr>
                                            <th>Jenis Kelamin</th>
                                            <th>Jumlah Kunjungan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($data["jk"] as $val) :
                                            ?>
                                            <tr>
                                                <th><?php echo $val->jk ?></th>
                                                <td><?php echo $val->jumlah . " Orang" ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div> <!-- end col -->
                <div class="col-xl-12">
                    <div class="card m-b-30">
                        <div class="card-body">
                            <h4 class="mt-0 header-title">Berdasarkan Pekerjaan</h4>
                            <div class="table-responsive">
                                <table class="table table-bordered mb-0">
                                    <thead>
                                        <tr>
                                            <th>Pekerjaan</th>
                                            <th>Jumlah Kunjungan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($data["job"] as $val) :
                                            ?>
                                            <tr>
                                                <th><?php echo $val->pekerjaan ?></th>
                                                <td><?php echo $val->jumlah . " Orang" ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div> <!-- end col -->
            </div>
            <div class="col-xl-6">
                <div class="card m-b-30">
                    <div class="card-body">
                        <h4 class="mt-0 header-title">Berdasarkan Kecamatan</h4>
                        <div class="table-responsive">
                            <table class="table table-bordered mb-0">
                                <thead>
                                    <tr>
                                        <th>Kecamatan</th>
                                        <th>Jumlah Kunjungan</th>
                                        <th>Kecamatan</th>
                                        <th>Jumlah Kunjungan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $num = 0;
                                    ?>
                                    <?php foreach ($data["kec"] as $val) :
                                        ?>
                                        <?php if ($num % 2 == 0) : ?>
                                            <tr>
                                                <th><?php echo $val->kecamatan ?></th>
                                                <td><?php echo $val->jumlah . " Orang" ?></td>
                                            <?php else : ?>
                                                <th><?php echo $val->kecamatan ?></th>
                                                <td><?php echo $val->jumlah . " Orang" ?></td>
                                            <?php endif; ?>
                                            <?php if ($num % 2 == 1) : ?>
                                            </tr>
                                        <?php endif; ?>
                                        <?php $num++; ?>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div> <!-- end col -->
        </div>
    </div>
</div>
<hr>