<?php
/**
 * @Author  : Sofian Eka Sandra<fianeka.me@gmail.com>
 * @Date    : 12/05/17 - 3:32 AM
 */

class KeperluanModel extends Model{
    protected $tableName = "keperluan";
    public function get($params = "") {
        $data = array();
        $ksr = $this->db->getAll($this->tableName)->toObject();
        foreach($ksr as $val) {
            $total = $this->db->getWhere('keperluan', array('id_keperluan' => $val->id_keperluan))->numRows();
            $val->total = $total;
            array_push($data, $val);
        }
        return $data;
    }
}
?>
