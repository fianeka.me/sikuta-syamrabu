<?php

use \modules\controllers\MainController;

class BukutamuController extends MainController
{

  public function index()
  {
    $this->model('keperluan');
    // $kpl = $this->keperluan->get();
    $kpl = $this->keperluan->getWhere(array('status' => 1));
    $this->model('bukutamu');
    $error      = array();
    $success    = null;
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
      $nrm        = isset($_POST["nrm"]) ? $_POST["nrm"] : "";
      $nama       = isset($_POST["nama"]) ? $_POST["nama"] : "";
      $jk         = isset($_POST["jk"]) ? $_POST["jk"] : "";
      $tgllahir   = isset($_POST["tgllahir"]) ? $_POST["tgllahir"] : "";
      $notel      = isset($_POST["notel"]) ? $_POST["notel"] : "";
      $kecamatan  = isset($_POST["kec"]) ? $_POST["kec"] : "";
      $pekerjaan  = isset($_POST["pekerjaan"]) ? $_POST["pekerjaan"] : "";
      $alamat     = isset($_POST["alamat"]) ? $_POST["alamat"] : "";
      $keperluan  = isset($_POST["keperluan"]) ? $_POST["keperluan"] : "";
      $ket        = isset($_POST["ket"]) ? $_POST["ket"] : "";
      $ttd        = isset($_POST["ttd"]) ? $_POST["ttd"] : "";
      if (count($error) == 0) {
        $insert = $this->bukutamu->insert(
          array(
            "nrm" => $nrm,
            "nama" => $nama,
            "jk" => $jk,
            "tgl_lahir" => $tgllahir,
            "notel" => $notel,
            "kecamatan" => $kecamatan,
            "pekerjaan" => $pekerjaan,
            "alamat" => $alamat,
            "id_keperluan" => $keperluan,
            "waktu_kunjungan" => date("Y-m-d"),
            "jam_kunjungan" => date("H:i:s"),
            "keterangan" => $ket,
            "ttd" => $ttd
          )
        );
        if ($insert) {
          $success = "Data Kunjungan Berhasil Ditambahkan";
        }
      }
    }
    $this->template('bukutamu', array('keperluan' => $kpl, 'error' => $error, 'success' => $success));
    // $this->template('bukutamu', array('keperluan' => $keperluan));
  }
}
