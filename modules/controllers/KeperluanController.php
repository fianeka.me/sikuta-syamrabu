<?php

use \modules\controllers\MainController;

class KeperluanController extends MainController
{
  public function index()
  {
    $this->model('keperluan');
    $data = $this->keperluan->get();
    $this->template('keperluan', array('kpl' => $data));
  }

  public function insert() {
      $this->model('keperluan');
      $error      = array();
      $success    = null;
      if($_SERVER["REQUEST_METHOD"] == "POST") {
          $des      = isset($_POST["des"]) ? $_POST["des"] : "";
          $umur      = isset($_POST["umur"]) ? $_POST["umur"] : "0";
          if(count($error) == 0) {
              $insert = $this->keperluan->insert(
                  array(
                    'keperluan' => $des,
                    'minimalumur' => $umur
                  )
              );
              if($insert) {
                  $success = "Data Berhasil di ditambahkan.";
              }
          }
      }
      $this->redirect("keperluan");
  }

  public function delete() {
      $id = isset($_GET["id"]) ? $_GET["id"] : 0;
      $this->model('keperluan');
      $delete = $this->keperluan->delete(array('id_keperluan' => $id));
      if($delete) {
      $this->redirect("keperluan");
      }
  }

  public function update() {
      $this->model('keperluan');
      if($_SERVER["REQUEST_METHOD"] == "POST") {
        $des      = isset($_POST["des"]) ? $_POST["des"] : "";
        $umur      = isset($_POST["umur"]) ? $_POST["umur"] : "0";
        $id      = isset($_POST["id"]) ? $_POST["id"] : "";
        if(count($error) == 0) {
          $dataUpdate = array(
            'keperluan' => $des,
            'minimalumur' => $umur
          );
          $update = $this->keperluan->update($dataUpdate, array('id_keperluan' => $id));
          if($update) {
            $success = "Data berhasil di rubah.";
          }
        }
      }
      $this->redirect("keperluan");
    }

  public function setActive(){
    $id = isset($_GET["id"]) ? $_GET["id"] : 0;
    $this->model('keperluan');
    $update = $this->keperluan->update(array('status' => "1"), array('id_keperluan' => $id));
    if($update) {
        $this->redirect("keperluan");
    }
  }

  public function setNonActive(){
    $id = isset($_GET["id"]) ? $_GET["id"] : 0;
    $this->model('keperluan');
    $update = $this->keperluan->update(array('status' => "0"), array('id_keperluan' => $id));
    if($update) {
      $this->redirect("keperluan");
    }
  }


}
