<?php

use \modules\controllers\MainController;

class AdminController extends MainController
{
  public function index()
  {
    $this->model('bukutamu');
    $data = $this->bukutamu->getJoin(
      array('keperluan'),
      array(
        'bukutamu.id_keperluan' => 'keperluan.id_keperluan'
      ),
      'JOIN',
      '',
      'order by bukutamu.id desc LIMIT 6'
    );
    $jk = $this->bukutamu->getCustom(
      "jk, count(id) as jumlah ",
      "WHERE jk is not null GROUP BY jk"
    );
    $kec = $this->bukutamu->getCustom(
      "kecamatan, count(id) as jumlah ",
      "WHERE kecamatan is not null GROUP BY kecamatan"
    );
    $job = $this->bukutamu->getCustom(
      "pekerjaan, count(id) as jumlah ",
      "WHERE pekerjaan is not null GROUP BY pekerjaan"
    );
    $this->template('laporan', array('data' => $data, 'jk' => $jk, 'kec' => $kec, 'job' => $job));
  }

  public function bukutamu()
  {
    $this->model('bukutamu');
    $data = $this->bukutamu->getJoin(
      array('keperluan'),
      array(
        'bukutamu.id_keperluan' => 'keperluan.id_keperluan'
      ),
      'JOIN',
      '',
      'order by bukutamu.id DESC'
    );
    $this->template('admin', array('bukutamu' => $data));
  }

  public function byDate()
  {
    $this->model('bukutamu');
    $data = array();
    $tgl = "";
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
      $tgl       = isset($_POST["tgl"]) ? $_POST["tgl"] : "";
      $data = $this->bukutamu->getJoin(
        array('keperluan'),
        array(
          'bukutamu.id_keperluan' => 'keperluan.id_keperluan'
        ),
        'JOIN',
        array(
          'bukutamu.waktu_kunjungan' => $tgl
        ),
        'order by bukutamu.id DESC'
      );
    }
    $this->template('kunjunganwaktu', array('bukutamu' => $data, 'tgl' => $tgl));
  }

  public function cetak()
  {
    $this->model('bukutamu');
    $data = array();
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
      $tgl       = isset($_POST["tgl"]) ? $_POST["tgl"] : "";
      $kec       = isset($_POST["kec"]) ? $_POST["kec"] : "";
      $job       = isset($_POST["pekerjaan"]) ? $_POST["pekerjaan"] : "";
      $cek = array();
      if(!empty($tgl)){
        $cek['bukutamu.waktu_kunjungan'] = $tgl;
      }
      if($kec !== "All"){
        $cek['bukutamu.kecamatan'] = $kec;
      }
      if($job !== "All"){
        $cek['bukutamu.pekerjaan'] = $job;
      }
      $data = $this->bukutamu->getJoin(
        array('keperluan'),
        array(
          'bukutamu.id_keperluan' => 'keperluan.id_keperluan'
        ),
        'JOIN',
        $cek,
        'order by bukutamu.id DESC'
      );
    }
    $this->template('cetak', array('bukutamu' => $data));
  }

  public function byCity()
  {
    $this->model('bukutamu');
    $data = array();
    $kec = "";
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
      $kec       = isset($_POST["kec"]) ? $_POST["kec"] : "";
      $data = $this->bukutamu->getJoin(
        array('keperluan'),
        array(
          'bukutamu.id_keperluan' => 'keperluan.id_keperluan'
        ),
        'JOIN',
        array(
          'bukutamu.kecamatan' => $kec
        ),
        'order by bukutamu.id DESC'
      );
    }
    $this->template('kunjungankecamatan', array('bukutamu' => $data, 'kec' => $kec));
  }

  public function byJob()
  {
    $this->model('bukutamu');
    $data = array();
    $job = "";
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
      $job       = isset($_POST["pekerjaan"]) ? $_POST["pekerjaan"] : "";
      $data = $this->bukutamu->getJoin(
        array('keperluan'),
        array(
          'bukutamu.id_keperluan' => 'keperluan.id_keperluan'
        ),
        'JOIN',
        array(
          'bukutamu.pekerjaan' => $job
        ),
        'order by bukutamu.id DESC'
      );
    }
    $this->template('kunjunganpekerjaan', array('bukutamu' => $data, 'job' => $job));
  }
}
