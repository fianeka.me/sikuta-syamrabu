<?php

namespace modules\controllers;

use \Controller;

class MainController extends Controller
{
    protected $login;
    public function __construct()
    {
      // uncomment for auth
        // $this->login = isset($_SESSION["login"]) ? $_SESSION["login"] : '';
        // $this->type = isset($_SESSION["type"]) ? $_SESSION["type"] : '';
        // if ($this->type!="0") {
        //     $this->redirect(SITE_URL . "?page=login");
        // }
        // if (!$this->login && $this->type!="0") {
        //     $this->redirect(SITE_URL . "?page=login");
        // }
    }

    protected function template($viewName, $data = array())
    {
        $view = $this->view('template');
        $view->bind('viewName', $viewName);
        $view->bind('data', array_merge($data, array('login' => $this->login)));
    }
}
