<?php
    include '../../library/utils.class.php';

    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "syam_bukutamu";
    // membuat koneksi
    $koneksi = new mysqli($servername, $username, $password, $dbname);
    // melakukan pengecekan koneksi
    if ($koneksi->connect_error) {
        die("Connection failed: " . $koneksi->connect_error);
    }
    if($_POST['rowid']) {
        $id = $_POST['rowid'];
        $sql = "SELECT id, nama, alamat, nama_kecamatan, tgl_lahir, telpon, pekerjaan, kelamin FROM smis_rg_patient WHERE id LIKE '%$id%' or nama LIKE '%$id%'";
        $result = $koneksi->query($sql);
        ?>
        <table id="datatable" class="table table-bordered dt-responsive" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
            <thead class="text-center">
                <tr>
                    <th>NRM</th>
                    <th>NAMA</th>
                    <th>ALAMAT</th>
                    <th>KEC</th>
                    <th>AKSI</th>
                </tr>
            </thead>
        <?php
        foreach ($result as $val) { ?>
              <tr class="text-center">
                <td><?php echo $val['id']; ?></td>
                <td><?php echo $val['nama']; ?></td>
                <td><?php echo $val['alamat']; ?></td>
                <td><?php echo $val['nama_kecamatan']; ?></td>
                <td>
                  <div id="act">

                  <a type="button" href="" class="btn btn-success waves-effect waves-light" data-toggle="modal" data-target=".bs-example-modal-center1" data-id="<?php echo $val['id'].'|'.$val['nama'].'|'.$val['alamat'].'|'.$val['nama_kecamatan'].'|'.$val['tgl_lahir'].'|'.$val['telpon'].'|'.$val['pekerjaan'].'|'.$val['kelamin'] ?>">Pilih</a>
                </div>
                </td>
              </tr>
        <?php
        }
        ?>
      </table>
    <?php
    }
    $koneksi->close();
?>
<script type="text/javascript">

    function cekKecamatan(selectedkec) {
      if (selectedkec == " ") {
        return  "Daerah Lain";
      }
      var toFind = selectedkec.toLowerCase().replace(/\s/g, '');
      var kec = ["Arosbaya", "Bangkalan", "Blega","Burneh", "Galis", "Geger","Kamal", "Klampis", "Kokop","Konang", "Kwanyar", "Labang","Modung", "Sepulu", "Socah","Tanah Merah", "Tanjung Bumi", "Tragah"];
      var reformat = [];
      for (var i = 0; i < 17; i++) {
        reformat.push(kec[i].toLowerCase().replace(/\s/g, ''));
      }
      var idx = reformat.indexOf(toFind);
      return kec[idx];
    }

    function cekPekerjaan(job) {
      if (job == " " || job == "%" || job == "LAINNYA") {
        return "Lain-Lain";
      }
      var toFind = job.toLowerCase().replace(/\s/g, '');
      var jobs = ["Pegawai Negeri Sipil", "Swasta", "TNI","POLRI", "Karyawan BUMN", "Pelajar","Mahasiswa", "Tani", "Buruh","Nelayan"];
      var reformat = [];
      for (var i = 0; i < 10; i++) {
        reformat.push(jobs[i].toLowerCase().replace(/\s/g, ''));
      }
      reformat[0] = "pns";
      var idx = reformat.indexOf(toFind);
      return jobs[idx];
    }

    $(document).ready(function(){
      $("#act a").click(function(){
            var rowid = $(this).data("id");
            var res = rowid.split("|");
            console.log(res);
            $('#nrm').val(res[0]);
            $('#nama').val(res[1]);
            $('#alamat').val(res[2]);
            $('#kec').val(cekKecamatan(res[3]));
            $('#tgl').val(res[4]);
            $('#notel').val(res[5]);
            $('#pekerjaan').val(cekPekerjaan(res[6]));
            var kelamin = res[7] == 1 ? "Perempuan" : "Laki-Laki";
            $('#jk').val(kelamin);
            $('#modalnya').modal('toggle');
            setKeperluan()
         });
    });
</script>
<script type="text/javascript">
$(document).ready(function() {
    // $('#datatable').DataTable();

    $('#datatable').dataTable( {
        "order": [],
        "lengthMenu": [5, 25, 50, 100, 500],
        "pageLength": 5
    } );

    //Buttons examples
    var table = $('#datatable-buttons').DataTable({
        lengthChange: false,
        buttons: ['copy', 'excel', 'pdf', 'colvis']
    });

    table.buttons().container()
        .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');
} );
</script>
