<?php
    include '../../library/utils.class.php';

    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "syam_bukutamu";
    // membuat koneksi
    $koneksi = new mysqli($servername, $username, $password, $dbname);
    // melakukan pengecekan koneksi
    if ($koneksi->connect_error) {
        die("Connection failed: " . $koneksi->connect_error);
    }
    if($_POST['rowid']) {
        $id = $_POST['rowid'];
        $sql = "SELECT * FROM bukutamu JOIN keperluan ON bukutamu.id_keperluan = keperluan.id_keperluan WHERE bukutamu.id = $id";
        $result = $koneksi->query($sql);
        foreach ($result as $val) { ?>
            <table class="table">
              <tr>
                <td>NRM</td>
                <td>:</td>
                <td><?php echo $val['nrm']; ?></td>
              </tr>
              <tr>
                <td>NAMA</td>
                <td>:</td>
                <td><?php echo $val['nama']; ?></td>
              </tr>
              <tr>
                <td>UMUR</td>
                <td>:</td>
                <td>
                  <?php
                  $interval = date_diff(date_create(), date_create($val['tgl_lahir']));
                  echo $interval->format("%Y Tahun, %M Bulan, %d Hari");
                  ?>
                </td>
              </tr>
              <tr>
                <td>JENIS KELAMIN</td>
                <td>:</td>
                <td><?php echo $val['jk'] ?></td>
              </tr>
              <tr>
                <td>PEKERJAAN</td>
                <td>:</td>
                <td><?php echo $val['pekerjaan'] ?></td>
              </tr>
              <tr>
                <td>NOMOR TELPHONE</td>
                <td>:</td>
                <td><?php echo $val['notel'] ?></td>
              </tr>
              <tr>
                <td>ALAMAT<br></td>
                <td>:</td>
                <td><?php echo $val['alamat'] ?>, <?php echo $val['kecamatan'] ?></td>
              </tr>
              <tr>
                <td>TGL KUNJUNGAN</td>
                <td>:</td>
                <td><?php echo tanggal_indo($val['waktu_kunjungan'], true) ?></td>
              </tr>
              <tr>
                <td>JAM KUNJUNGAN</td>
                <td>:</td>
                <td><?php echo jam_indo($val['jam_kunjungan']) ?></td>
              </tr>
              <tr>
                <td>KEPERLUAN<br></td>
                <td>:</td>
                <td><?php echo $val['keperluan'] ?></td>
              </tr>
              <tr>
                <td>TERTANDA<br></td>
                <td>:</td>
                <td><img src="<?php echo $val['ttd'] ?>" height="48"/></td>
              </tr>
            </table>
        <?php

        }
    }
    $koneksi->close();
?>
